//Functions
function sayHello(){
	console.log("This is from sayHello function");
	console.log("Hello");
}

//Functions with parameter
function sayHello2(name){
	console.log("This is from sayHello2 function");
	console.log("Hello " + name);
}

//Calling a function with an Arguement
sayHello();
sayHello2("Zuitt");
sayHello2("Sean Mark Mira");

function sayHello3(x,y){
	console.log("This is from sayHello3 function");
	console.log("Hello " + x +" " + y);
}

sayHello3("Sean", "Mark");

function addNum(x,y){
	sum = x + y;
	console.log(sum);
}

addNum(5,5);